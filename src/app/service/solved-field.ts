import { OneToNine } from "app/service/digit";
import { allPositionsInBox, allPositionsInColumn, allPositionsInRow, PosIter, Position } from "app/service/position";
import { Reason } from "app/service/sudoku";
import { assertNever } from "app/util";

export class SolvedField {
  constructor(readonly position: Position, readonly digit: OneToNine, readonly reason: Reason) { }

  * allPositionsForReason(): PosIter {
    switch (this.reason) {
      case "OnlyPossiblePositionForColumn":
        yield* allPositionsInColumn(this.position);
        break;
      case "OnlyPossiblePositionForRow":
        yield* allPositionsInRow(this.position);
        break;
      case "OnlyPossiblePositionForBox":
        yield* allPositionsInBox(this.position);
        break;
      case "OnlyPossibleDigitForCell":
        //no positions to show for this reason
        break;
      default:
        return assertNever(this.reason);
    }
  }
}
