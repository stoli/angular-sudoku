export type SetDigitResultKind = undefined | "warning" | "solved";

export type SetDigitMessage = undefined | "digitNotCorrect" | "digitNotPossible" | "solved" | "solvedDigit";

export type FieldState = undefined | "warning" |
  "solvedField" | "solvedFieldInSolvedGroup" | "solvedGroup" | "solvedFieldInSolvedSudoku" | "solvedSudoku";

export class SetDigitResult {
  constructor(
    readonly kind: SetDigitResultKind,
    readonly message: SetDigitMessage,
    readonly fieldStates: FieldState[][],
    readonly speedPoints: number,
    readonly bonusPoints: number = 0,
  ) { }

  get points(): number {
    return this.speedPoints + this.bonusPoints;
  }
}
