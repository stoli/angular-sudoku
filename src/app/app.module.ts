import { HttpClient, HttpClientModule } from "@angular/common/http";
import { NgModule } from "@angular/core";
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { AngularFirestoreModule } from "@angular/fire/firestore";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonModule } from "@angular/material/button";
import { ErrorStateMatcher, ShowOnDirtyErrorStateMatcher } from "@angular/material/core";
import { MatDialogModule } from "@angular/material/dialog";
import { MatGridListModule } from "@angular/material/grid-list";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatMenuModule } from "@angular/material/menu";
import { MatSidenavModule } from "@angular/material/sidenav";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTabsModule } from "@angular/material/tabs";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatTooltipModule } from "@angular/material/tooltip";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { TranslateLoader, TranslateModule } from "@ngx-translate/core";
import { TranslateHttpLoader } from "@ngx-translate/http-loader";
import { AppComponent } from "app/app.component";
import { ConfirmComponent } from "app/confirm/confirm.component";
import { DigitComponent } from "app/digit/digit.component";
import { GridComponent } from "app/grid/grid.component";
import { LoginComponent } from "app/login/login.component";
import { SidenavComponent } from "app/sidenav/sidenav.component";
import { ToolbarComponent } from "app/toolbar/toolbar.component";

// AoT requires an exported function for factories
export function httpLoaderFactory(http: HttpClient): TranslateLoader {
  return new TranslateHttpLoader(http, "./assets/i18n/");
}

@NgModule({
  bootstrap: [AppComponent],
  declarations: [
    AppComponent,
    ConfirmComponent,
    LoginComponent,
    DigitComponent,
    GridComponent,
    SidenavComponent,
    ToolbarComponent,
  ],
  entryComponents: [
    ConfirmComponent,
    LoginComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatSidenavModule,
    MatToolbarModule,
    MatIconModule,
    MatSnackBarModule,
    MatDialogModule,
    MatTabsModule,
    MatInputModule,
    MatTooltipModule,
    MatMenuModule,
    MatGridListModule,
    HttpClientModule,
    TranslateModule.forRoot({
      loader: {
        deps: [HttpClient],
        provide: TranslateLoader,
        useFactory: httpLoaderFactory,
      },
    }),
    AngularFireModule.initializeApp({
      apiKey: "AIzaSyDLtCpl-B0yD4_Nr-ulcokswM9PKnK05IM",
      authDomain: "angular-sudoku.firebaseapp.com",
      databaseURL: "https://angular-sudoku.firebaseio.com",
      messagingSenderId: "995445029311",
      projectId: "angular-sudoku",
      storageBucket: "angular-sudoku.appspot.com",
    }),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    //    ServiceWorkerModule.register("/ngsw-worker.js",
    //      { enabled: environment.production }),
  ],
  providers: [
    { provide: ErrorStateMatcher, useClass: ShowOnDirtyErrorStateMatcher }],
})
export class AppModule { }
