# Angular Sudoku

Play the game at https://stoli.gitlab.io/angular-sudoku/.

## Features

- Solve Sudokus in different levels.
- Let help yourself, if you are stuck.
- Choose a display theme.
- Play offline.
- Multiple languages available.
- Use your mobile or desktop browser.
- Add a bookmark to your home screen and enjoy a native look&feel.
- Log-in and store the current game, score and settings across all devices.


## Build

Angular Sudoku is based on Angular CLI. If you have not already installed Angular CLI, follow the steps on https://github.com/angular/angular-cli/blob/master/packages/angular/cli/README.md

Afterwards just:

- clone (or download) the sources at https://gitlab.com/Stenzi/angular-sudoku.git
- open a console
- `cd angular-sudoku`
- `npm install`
- `ng serve -o`
